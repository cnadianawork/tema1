package proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: persoane.proto")
public final class PersoaneGrpc {

  private PersoaneGrpc() {}

  public static final String SERVICE_NAME = "Persoane";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<proto.PersoaneOuterClass.CerereDate,
      proto.PersoaneOuterClass.PrimireDate> getGetDataMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getData",
      requestType = proto.PersoaneOuterClass.CerereDate.class,
      responseType = proto.PersoaneOuterClass.PrimireDate.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<proto.PersoaneOuterClass.CerereDate,
      proto.PersoaneOuterClass.PrimireDate> getGetDataMethod() {
    io.grpc.MethodDescriptor<proto.PersoaneOuterClass.CerereDate, proto.PersoaneOuterClass.PrimireDate> getGetDataMethod;
    if ((getGetDataMethod = PersoaneGrpc.getGetDataMethod) == null) {
      synchronized (PersoaneGrpc.class) {
        if ((getGetDataMethod = PersoaneGrpc.getGetDataMethod) == null) {
          PersoaneGrpc.getGetDataMethod = getGetDataMethod = 
              io.grpc.MethodDescriptor.<proto.PersoaneOuterClass.CerereDate, proto.PersoaneOuterClass.PrimireDate>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Persoane", "getData"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.PersoaneOuterClass.CerereDate.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  proto.PersoaneOuterClass.PrimireDate.getDefaultInstance()))
                  .setSchemaDescriptor(new PersoaneMethodDescriptorSupplier("getData"))
                  .build();
          }
        }
     }
     return getGetDataMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PersoaneStub newStub(io.grpc.Channel channel) {
    return new PersoaneStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PersoaneBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new PersoaneBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PersoaneFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new PersoaneFutureStub(channel);
  }

  /**
   */
  public static abstract class PersoaneImplBase implements io.grpc.BindableService {

    /**
     */
    public void getData(proto.PersoaneOuterClass.CerereDate request,
        io.grpc.stub.StreamObserver<proto.PersoaneOuterClass.PrimireDate> responseObserver) {
      asyncUnimplementedUnaryCall(getGetDataMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetDataMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                proto.PersoaneOuterClass.CerereDate,
                proto.PersoaneOuterClass.PrimireDate>(
                  this, METHODID_GET_DATA)))
          .build();
    }
  }

  /**
   */
  public static final class PersoaneStub extends io.grpc.stub.AbstractStub<PersoaneStub> {
    private PersoaneStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoaneStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoaneStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoaneStub(channel, callOptions);
    }

    /**
     */
    public void getData(proto.PersoaneOuterClass.CerereDate request,
        io.grpc.stub.StreamObserver<proto.PersoaneOuterClass.PrimireDate> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetDataMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class PersoaneBlockingStub extends io.grpc.stub.AbstractStub<PersoaneBlockingStub> {
    private PersoaneBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoaneBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoaneBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoaneBlockingStub(channel, callOptions);
    }

    /**
     */
    public proto.PersoaneOuterClass.PrimireDate getData(proto.PersoaneOuterClass.CerereDate request) {
      return blockingUnaryCall(
          getChannel(), getGetDataMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class PersoaneFutureStub extends io.grpc.stub.AbstractStub<PersoaneFutureStub> {
    private PersoaneFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private PersoaneFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PersoaneFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new PersoaneFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<proto.PersoaneOuterClass.PrimireDate> getData(
        proto.PersoaneOuterClass.CerereDate request) {
      return futureUnaryCall(
          getChannel().newCall(getGetDataMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_DATA = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PersoaneImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PersoaneImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_DATA:
          serviceImpl.getData((proto.PersoaneOuterClass.CerereDate) request,
              (io.grpc.stub.StreamObserver<proto.PersoaneOuterClass.PrimireDate>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PersoaneBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PersoaneBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return proto.PersoaneOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Persoane");
    }
  }

  private static final class PersoaneFileDescriptorSupplier
      extends PersoaneBaseDescriptorSupplier {
    PersoaneFileDescriptorSupplier() {}
  }

  private static final class PersoaneMethodDescriptorSupplier
      extends PersoaneBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PersoaneMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PersoaneGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PersoaneFileDescriptorSupplier())
              .addMethod(getGetDataMethod())
              .build();
        }
      }
    }
    return result;
  }
}
