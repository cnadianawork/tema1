import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.PersoaneGrpc;
import proto.PersoaneOuterClass;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",
                8999).usePlaintext().build();
        PersoaneGrpc.PersoaneBlockingStub personStub = PersoaneGrpc.newBlockingStub(channel);
        Scanner scanner = new Scanner(System.in);
        boolean isRunning = true;
        int integer;
        int persoana=1;
        System.out.println("Introduceti numarul de persoane: ");
        integer=scanner.nextInt();
        while (isRunning && integer!=0) {
            integer--;
            System.out.println("Introduceti datele persoanei "+persoana+": ");
            System.out.println("Nume: ");
            String nume = scanner.next();
            System.out.println("CNP: ");
            String cnp = scanner.next();
            PersoaneOuterClass.PrimireDate date = personStub.getData(PersoaneOuterClass.CerereDate.newBuilder().setNume(nume).setCnp(cnp).build());
            persoana++;
        }
        System.out.println("Datele introduse au fost inregistrate cu succes!");
        scanner.close();
        channel.shutdown();

    }

}


