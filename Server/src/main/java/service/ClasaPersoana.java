package service;
import io.grpc.stub.StreamObserver;
import proto.PersoaneGrpc;
import proto.Persoana;
import java.time.Year;


public class ClasaPersoana  extends PersoaneGrpc.PersoaneImplBase
{

    public static int getAnulNasterii(String cnp) {


        Integer anul_nasterii;
        long cnp_= Long.parseLong(cnp);
        Integer numar=(int) (cnp_/(Math.pow(10, 10)));
        Integer an=numar%100;

        if(an>=21)
        {
            anul_nasterii=1900+an;
        }
        else anul_nasterii=2000+an;

        return anul_nasterii;


    }


    public static String getVarsta(String cnp) {


        int anul_nasterii=getAnulNasterii(cnp);
        int an_curent= Year.now().getValue();
        int varsta= an_curent-anul_nasterii;
        String var=String.valueOf(varsta);
        return var;

    }

    public static String getGen(String cnp) {

        String gen = new String();
      int var=cnp.charAt(0);
      //in ascii numerele de la 0 la 9 sunt cuprinse intre 48 si 57
        if(var%2 ==0) gen="Feminin";
        else gen="Masculin";
        return gen;

    }

    @Override
    public void getData(Persoana.CerereDate request, StreamObserver<Persoana.PrimireDate> responseObserver) {
        Persoana.PrimireDate response = Persoana.PrimireDate.newBuilder().setNume(request.getNume()).
                setGen(getGen(request.getCnp())).setVarsta(getVarsta(request.getCnp())).build();
        System.out.println("Nume: " + request.getNume() + ", gen: " + response.getGen() +
                ", varsta: " + response.getVarsta());

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }



}
